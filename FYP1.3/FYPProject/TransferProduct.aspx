﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TransferProduct.aspx.cs" Inherits="TransferProduct" %>

<%@ Register Src="~/Controls/PublicReference.ascx" TagPrefix="uc1" TagName="PublicReference" %>
<%@ Register Src="~/Controls/Menu.ascx" TagPrefix="uc1" TagName="Menu" %>
<%@ Register Src="~/Controls/Footer.ascx" TagPrefix="uc1" TagName="Footer" %>
<%@ Register Src="~/Controls/CountryControl.ascx" TagPrefix="uc1" TagName="CountryControl" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title> 
        <uc1:PublicReference runat="server" id="PublicReference" />
    <script>
        $(function () {

            // Check Login
            if (getCookie("staffID") == "") {
                window.location = "Login.aspx";
            }
            else {
                var staffName = getCookie("staffName");
                $("#staffNameUserBar").html("<a href=\"#\">Welcome, " + staffName + "</a>   ");
                $("#checkLogin").attr("href", "Login.aspx");
                $("#checkLogin").text("Logout");
            }
            $("#checkLogin").click(function () {
                setCookie("staffID", "", 0)

            });
            // /Check Login

            $("#quantity").focusout(function () {
                if ($("#quantity").val() < 0)
                {
                    $("#quantity").val("0");
                }
                if (parseInt($("#quantity").val()) > parseInt($("#ShopQuantity").val()))
                {
                    alert("Insufficient stock!");
                    $("#quantity").val("0");
                }
            });

            $("#productID").focusout(function () { 

                var productID = $("#productID").val();
                if (productID != "") {
                    ajaxSubmit({ // Search productName by productID
                        action: "SearchProduct",
                        productID: productID,
                    }, function (data) {
                        if (data.message == "success") {
                            $("#productBrand").val(data.productBrand);
                            $("#productName").val(data.productName);
                        }
                        else {
                            alert("Product not found.");
                            $("#productID").val("");
                            $("#productName").val("");
                            $("#productBrand").val("");
                        }

                    });

                    if ($("#ShopID").val() != "") {
                        ajaxSubmit({
                            action: "SearchShopStocking",
                            shopID: $("#ShopID").val(),
                            productID: productID,
                        }, function (data) {
                            if (data.message == "success") {
                                $("#ShopQuantity").val(data.quantity);
                            }
                        });
                    }

                    if ($("#ToShopID").val() != "") {
                        ajaxSubmit({
                            action: "SearchShopStocking",
                            shopID: $("#ToShopID").val(),
                            productID: productID,
                        }, function (data) {
                            if (data.message == "success") {
                                $("#ToShopQuantity").val(data.quantity);
                            }
                        });
                    }
                }
                else {
                    $("#productName").val("");
                    $("#productBrand").val("");
                    $("#ShopQuantity").val("");
                    $("#ToShopQuantity").val("");
                }
            });
            $("#ShopID").focusout(function () {
                var shopID = $("#ShopID").val();
                if(shopID != ""){
                    if(shopID != "" && shopID != $("#ToShopID").val()){
                        ajaxSubmit({
                            action: "SearchShop",
                            shopID: shopID,
                        }, function (data) {
                            if (data.message == "success")
                            {
                                $("#ShopAddress").val(data.shopAddress);
                            }
                            else {
                                alert("Shop not found.");
                                $("#ShopID").val("");
                                $("#ShopAddress").val("");
                                $("#ShopQuantity").val("");
                                
                            }
                        });
                    }
                    else if (shopID == $("#ToShopID").val())
                    {
                        alert("Cannot transfer product to same shop.");
                        $("#ShopID").val("");
                        $("#ShopAddress").val("");
                        $("#ShopQuantity").val("");
                    }
                }
                else{
                $("#ShopAddress").val("");
                $("#ShopQuantity").val("");
                }
                            var productID = $("#productID").val();
                            ajaxSubmit({
                                action: "SearchShopStocking",
                                shopID: shopID,
                                productID: productID,
                            }, function (data) {
                                if (data.message == "success") {
                                    $("#ShopQuantity").val(data.quantity);
                                    if (parseInt($("#ShopQuantity").val()) < parseInt($("#quantity").val()))
                                    {
                                        alert("Insufficient Stock!");
                                        $("#quantity").val("0");
                                    }
                                }
                                else {

                                }
                            });
                        
                    
            });


            $("#ToShopID").focusout(function () {
                var shopID = $("#ToShopID").val();
                if (shopID != "") {
                    if (shopID != "" && shopID != $("#ShopID").val()) {
                        ajaxSubmit({
                            action: "SearchShop",
                            shopID: shopID,
                        }, function (data) {
                            if (data.message == "success") {
                                $("#ToShopAddress").val(data.shopAddress);

                            }
                            else {
                                alert("Shop not found.");
                                $("#ToShopID").val("");
                                $("#ToShopAddress").val("");
                                $("#ToShopQuantity").val("");

                            }
                        });
                    }
                    else if (shopID == $("#ShopID").val()) {
                        alert("Cannot transfer product to same shop.")
                        $("#ToShopID").val("");
                        $("#ToShopAddress").val("");
                        $("#ToShopQuantity").val("");
                    }
                }
                else {
                    $("#ToShopAddress").val("");
                    $("#ToShopQuantity").val("");
                }
                        var productID = $("#productID").val();
                        ajaxSubmit({
                            action: "SearchShopStocking",
                            shopID: shopID,
                            productID: productID,
                        }, function (data) {
                            if (data.message == "success")
                            {
                                $("#ToShopQuantity").val(data.quantity);
                            }
                            
                        });
               
               
                
            });

            $("#submitBtn").click(function () {
                var productID = $("#productID").val();
                var quantity = 0;
                quantity = parseInt($("#quantity").val());
                var fromshopID = $("#ShopID").val();
                var toshopID = $("#ToShopID").val();

                var fromshopQuantity = 0;
                fromshopQuantity = parseInt($("#ShopQuantity").val());

                var fromshopfinalQuantity = 0;
                fromshopfinalQuantity = parseInt(fromshopQuantity) - parseInt(quantity);

                var toshopQuantity = 0;
                toshopQuantity = parseInt($("#ToShopQuantity").val());

                var toshopfinalQuantity = 0;
                toshopfinalQuantity = parseInt(toshopQuantity) + parseInt(quantity);

                if (productID == "" || $("productName").val() == "" || $("productBrand").val() == "" || quantity == "" ||
                    $("#ShopID").val() == "" || $("#ShopAddress").val() == "" || $("#ShopQuantity").val() == "" ||
                    $("#ToShopID").val() == "" || $("#ToShopAddress").val() == "" || $("#ToShopQuantity").val() == "") {
                    alert("Please fill in the form above!");
                }
                else
                    ajaxSubmit({
                        action: "TransferProduct",
                        productID: productID,
                        fromshopID: fromshopID,
                        toshopID: toshopID,
                        fromshopfinalQuantity: fromshopfinalQuantity,
                        toshopfinalQuantity: toshopfinalQuantity,
                    }, function (data) {
                        if (data.message == "success") {
                            alert("Transfer Success!");
                            window.location = "TransferProduct.aspx";
                        }
                        else {
                            alert("Transfer Failed!");
                        }
                    });
            });
            





        });
    </script>
</head>
<body>
        <div id="header">
        <uc1:Menu runat="server" id="Menu" />
    </div>
    <div id="body">
         <div id="navigationLocation">
            <h6><span style="padding-left: 5%">E-Sales > Product > Product Transfer</span></h6>
            <hr />
            <h2><span style="margin-left: 5%;">Product Transfer</span></h2>
        </div>

<div class="container" style="margin-top: 5%; margin-bottom: 5%;">

    <div class="row">
        <div class="form-group col-sm-4 col-sm-offset-2">
            <label for="ProductID">Product ID</label>
            <input type="text" class="form-control" id="productID" />
        </div>
        <div class="form-group col-sm-4">
            <label for="ProductName">Product Name</label>
            <input type="text" class="form-control" id="productName" disabled/>
        </div>
        
    </div>

    <div class="row">
        <div class="form-group col-sm-4 col-sm-offset-2">
            <label for="ProductBrand">Product Brand</label>
            <input type="text" class="form-control" id="productBrand" disabled/>
        </div>
        <div class="form-group col-sm-4">
            <label for="ProductID">Quantity</label>
            <input type="number" class="form-control" id="quantity" />
        </div>
    </div>







    <div class="row">
        <div class="form-group col-sm-4 col-sm-offset-2">
            <label for="ShopID">From Shop ID</label>
            <input type="text" class="form-control" id="ShopID" />
        </div>
        <div class="form-group col-sm-4">
            <label for="ShopID">From Shop Address</label>
            <input type="text" class="form-control" id="ShopAddress" disabled/>
        </div>
        <div class="form-group col-sm-2">
            <label for="ToShopQuantity">Quantity</label>
            <input type="text" class="form-control" id="ShopQuantity" disabled />
        </div>
    </div>

    <div class="row">
        <div class="form-group col-sm-4 col-sm-offset-2">
            <label for="ToShopID">To Shop ID</label>
            <input type="text" class="form-control" id="ToShopID" />
        </div>
        <div class="form-group col-sm-4">
            <label for="ToShopID">To Shop Address</label>
            <input type="text" class="form-control" id="ToShopAddress" disabled/>
        </div>
        <div class="form-group col-sm-2">
            <label for="ToShopQuantity">Quantity</label>
            <input type="text" class="form-control" id="ToShopQuantity" disabled />
        </div>
    </div>

                <div class="form-group col-sm-2 col-sm-offset-4">
                    <label for="spacing"></label>
                    <input type="button" id="submitBtn" value="Submit" class="form-control btn btn-primary" />
                </div>

                <div class="form-group col-sm-2">
                    <label for="spacing"></label>
                    <input type="button" id="backButton" value="Back" class="form-control btn btn-default" />
                </div>

    



        </div>

    </div>
    <div class="footer">
        <uc1:Footer runat="server" id="Footer1" />
    </div>




</body>
</html>