﻿using System;
using System.Collections.Generic;
using System.Web;

/// <summary>
/// Summary description for TransferProduct
/// </summary>
public class TransferProduct
{
    public static readonly string SystemMailAccount = System.Configuration.ConfigurationManager.AppSettings["SystemMailAccount"];
    public static readonly string SystemMailDisplayName = System.Configuration.ConfigurationManager.AppSettings["SystemMailDisplayName"];
    public static readonly string SMTPServer = System.Configuration.ConfigurationManager.AppSettings["SMTPServer"];
    public static readonly int SMTPPortNo = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["SMTPPortNo"]);
    //public static readonly string SystemMailPassword = System.Configuration.ConfigurationManager.AppSettings["SystemMailPassword"];
    public static string SupervisorMailAccount = "";
    public static string SupervisorMailDisplayName = "";

    private DatabaseAccess dbAccess = new DatabaseAccess();

    public bool transferProduct (TransferProductInfo transferProductInfo)
    {
        
       
        String sql = string.Format("UPDATE [esales].[dbo].[Shop_Stocking] SET quantity = '{0}' where shop_id = '{1}' AND product_id = '{2}'", transferProductInfo.fromShopFinalQuantity, transferProductInfo.fromShopID, transferProductInfo.productID);
        String sql2 = string.Format("UPDATE [esales].[dbo].[Shop_Stocking] SET quantity = '{0}' where shop_id = '{1}' AND product_id = '{2}'", transferProductInfo.toShopFinalQuantity, transferProductInfo.toShopID, transferProductInfo.productID);
        dbAccess.open();
        try
        {
            dbAccess.update(sql);
            dbAccess.update(sql2);
            return true;
        }
        catch (Exception ex)
        {
            return false;

        }
        finally
        {
            dbAccess.close();
        }
    }
}