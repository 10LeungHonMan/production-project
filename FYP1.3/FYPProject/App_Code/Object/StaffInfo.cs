﻿using System;
using System.Collections.Generic;
using System.Web;

/// <summary>
/// Summary description for StaffInfo
/// </summary>
public class StaffInfo
{
    public string staffID;
    public string staffName;
    public string staffGender;
    public string staffPassword;
}