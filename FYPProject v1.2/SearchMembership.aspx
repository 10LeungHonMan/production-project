﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SearchMembership.aspx.cs" Inherits="SearchMembership" %>

<%@ Register Src="~/Controls/Menu.ascx" TagPrefix="uc1" TagName="Menu" %>
<%@ Register Src="~/Controls/PublicReference.ascx" TagPrefix="uc1" TagName="PublicReference" %>
<%@ Register Src="~/Controls/Footer.ascx" TagPrefix="uc1" TagName="Footer" %>
<%@ Register Src="~/Controls/CountryControl.ascx" TagPrefix="uc1" TagName="CountryControl" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
     <uc1:PublicReference runat="server" ID="PublicReference" />

    <script>
        $(function () {
            $dateOfBirth = $("#dateOfBirth");
            $dateOfBirth.pickadate({
                format: 'yyyy-mm-dd',
                selectYears: true,
                selectMonths: true
            });
            // Check Login
            if (getCookie("staffID") == "") {
                window.location = "Login.aspx";
            }
            else {
                var staffName = getCookie("staffName");
                $("#staffNameUserBar").html("<a href=\"#\">Welcome, " + staffName + "</a>   ");
                $("#checkLogin").attr("href", "Login.aspx");
                $("#checkLogin").text("Logout");
            }
            $("#checkLogin").click(function () {
                setCookie("staffID", "", 0)

            });
            // /Check Login

            $(".results").hide();

            $("#submitBtn").click(function () {
                var membershipID = $("#membershipID").val();
                var contactNumber = $("#contactNumberTxt2").val();
                var email = $("#emailTxt2").val();
                if (membershipID == "" && contactNumber == "" && email == "") {
                    $(".noresults").show(300);
                    $(".results").hide(300);
                } else {
                    ajaxSubmit({
                        action: "SearchMembership",
                        membershipID: membershipID,
                        contactNumber: contactNumber,
                        email: email,
                    }, function (data) {
                        if (data.message == "success") {
                            $(".noresults").hide(300);
                            $(".results").show(300);
                            //$("#membershipID").val(data.membershipID);
                            $("#firstNameTxt").val(data.firstname);
                            $("#lastNameTxt").val(data.lastname);
                            $("#contactNumberTxt").val(data.contactNumber);
                            $("#dateOfBirth").val(data.dateofbirth);
                            $("#countryTxt").val(data.country);
                            $("#emailTxt").val(data.email);
                            $("#memberRole").val(data.memberRole);
                        } else {
                            $(".results").hide(300);
                            $(".noresults").show(300);
                        }
                    });
                }
            });

        });
    </script>
</head>
<body>
    <div id="header">
        <uc1:Menu runat="server" ID="Menu" />
    </div>
    <div id="body">
         <div id="navigationLocation">
            <h5><span style="padding-left: 5%"><a href="Home.aspx" style="text-decoration:none;">E-Sales</a> > <a href="SearchMembership.aspx">Member</a> > Search Membership</span></h5>
            <hr />
            <h2><span style="margin-left: 5%;">Search Membership</span></h2>
        </div>

        <div class="container">
            
            <div class="row">
                <div class="form-group col-sm-2 col-sm-offset-1">
                    <label for="MembershipID">Membership ID</label>
                    <input type="text" class="form-control" id="membershipID" />
                </div>
                <div class="form-group col-sm-4">
                    <label for="contactNumberTxt">Contact Number</label>
                    <input type="text" id="contactNumberTxt2" class="form-control"/>
                </div>
                <div class="form-group col-sm-4">
                    <label for="emailTxt">E-mail</label>
                    <input type="email" id="emailTxt2" class="form-control"/>
                </div>
            </div>
            
            <div class="noresult">
                <center><h1>No Result</h1></center>
            </div>
            <div class="results">
                <div class="container">
                    <div class="row">
                <div class="form-group col-sm-4 col-sm-offset-2">
                    <label for="firstnameTxt">First Name</label>
                    <input type="text" id="firstNameTxt" class="form-control"/>
                </div>
                <div class="form-group col-sm-4">
                    <label for="firstnameTxt">Last Name</label>
                    <input type="text" id="lastNameTxt" class="form-control"/>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-sm-4 col-sm-offset-2">
                    <label for="firstnameTxt">Contact Number</label>
                    <input type="text" id="contactNumberTxt" class="form-control"/>
                </div>
                <div class="form-group col-sm-4">
                    <label for="firstnameTxt">Date of Birth</label>
                    <input type="date" id="dateOfBirth" class="form-control"/>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-sm-4 col-sm-offset-2">
                    <label for="firstnameTxt">Country</label>
                    <input type="text" class="form-control" id="countryTxt"/>
                </div>
                <div class="form-group col-sm-4">
                    <label for="firstnameTxt">E-mail</label>
                    <input type="email" id="emailTxt" class="form-control"/>
                </div>
            </div>

            <div class="row">
                <div class="form-group col-sm-4 col-sm-offset-4">
                    <label for="memberroleTxt">Role</label>
                    <input type="text" id="memberRole" class="form-control" />
                </div>
            </div>
                </div>
            </div>

            <div class="form-group col-sm-2 col-sm-offset-4">
                    <label for="spacing"></label>
                    <input type="button" id="submitBtn" value="Submit" class="form-control btn btn-primary" />
                </div>

                <div class="form-group col-sm-2">
                    <label for="spacing"></label>
                    <input type="button" id="backButton" value="Back" class="form-control btn btn-default" />
                </div>

        </div>

    </div>
     <div class="footer">
        <uc1:Footer runat="server" id="Footer1" />
    </div>
</body>
</html>
