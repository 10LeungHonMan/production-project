﻿<%@ WebHandler Language="C#" Class="AjaxHandler" %>

using System;
using System.Web;
using System.Web.SessionState;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using Jayrock.Json;


public class AjaxHandler : IHttpHandler, IRequiresSessionState
{


    public AjaxHandler()
    {

    }

    public void ProcessRequest(HttpContext context)
    {
        HttpRequest request = context.Request;
        HttpResponse response = context.Response;
        System.Web.SessionState.HttpSessionState session = context.Session;

        string action = request.Form["action"].ToString();
        string result = "";

        try
        {

            switch (action)
            {
                case "Login":
                    result = login(request);
                    break;
                case "SearchProduct":
                    result = SearchProduct(request);
                    break;
                case "SearchShop":
                    result = SearchShop(request);
                    break;
                case "SearchShopStocking":
                    result = SearchShopStocking(request);
                    break;
                case "TransferProduct":
                    result = TransferProduct(request);
                    break;
                case "SearchProductByShopID":
                    result = SearchProductByID(request);
                    break;
                case "JoinMembership":
                    result = JoinMembership(request);
                    break;
                case "SearchMembership":
                    result = SearchMembership(request);
                    break;
                case "SubmitEntry":
                    result = SubmitEntry(request);
                    break;
            }
        }
        catch (Exception ex)
        {
            Console.Write(ex);
        }

        response.Clear();
        response.ContentType = "application/json;charset=UTF-8;";
        response.Write(result);
        response.End();
    }


    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

    public string SubmitEntry(HttpRequest request)
    {
            EntryInfo entryInfo = new EntryInfo();
            entryInfo.EntryItem = request.Form["entryItem"];
            entryInfo.StaffID = request.Form["staffID"];
            entryInfo.StaffName = request.Form["staffName"];
            entryInfo.CustomerRegion = request.Form["customerRegion"];
            entryInfo.CustomerAge = request.Form["customerAge"];
            entryInfo.membershipID = request.Form["membershipID"];
            entryInfo.discount = request.Form["discount"];
            entryInfo.finalPrice = request.Form["finalPrice"];
            entryInfo.lastEntryItemID = request.Form["lastentryid"];
            Entry entry = new Entry();
            bool status = entry.InsertEntry(entryInfo);
            if(status)
                return "{\"message\":\"success\"}";
            else
                return "{\"message\":\"failed\"}";
    }

    public string login(HttpRequest request)
    {
        StaffInfo staffInfo = new StaffInfo();
        staffInfo.staffID = request.Form["staffID"];
        staffInfo.staffPassword = request.Form["password"];
        Staff staff = new Staff();
        StaffInfo resultStaff = staff.login(staffInfo);
        if (resultStaff != null)
            return "{\"message\":\"success\", \"staffName\":\"" + resultStaff.staffName + "\", \"staffID\":\"" + resultStaff.staffID + "\"}";
        else
            return "{\"message\":\"failed\"}";
    }

    public string SearchProduct(HttpRequest request)
    {
        ProductInfo productInfo = new ProductInfo();
        productInfo.productID = request.Form["productID"];
        Product product = new Product();
        ProductInfo resultProduct = product.searchProduct(productInfo);
        if (resultProduct != null)
            return "{\"message\":\"success\", \"productName\":\"" + resultProduct.productName + "\", \"productID\":\"" + resultProduct.productID + "\", \"productBrand\":\"" + resultProduct.productBrand + "\", \"productPrice\":\"" + resultProduct.productPrice + "\", \"productQuantity\":\"" + resultProduct.productQuantity + "\" }";
        else
            return "{\"message\":\"failed\"}";
    }

    public string SearchProductByID(HttpRequest request)
    {
        ProductInfo productInfo = new ProductInfo();
        productInfo.productID = request.Form["productID"];
        productInfo.shopID = request.Form["shopID"];
        Product product = new Product();
        ProductInfo resultProduct = product.searchProductByID(productInfo);
        if (resultProduct != null)
            return "{\"message\":\"success\", \"productName\":\"" + resultProduct.productName + "\", \"productID\":\"" + resultProduct.productID + "\", \"productBrand\":\"" + resultProduct.productBrand + "\", \"productPrice\":\"" + resultProduct.productPrice + "\", \"productQuantity\":\"" + resultProduct.productQuantity + "\" }";
        else
            return "{\"message\":\"failed\"}";
    }

    public string SearchShop(HttpRequest request)
    {
        ShopInfo shopInfo = new ShopInfo();
        shopInfo.shopID = request.Form["shopID"];
        Shop shop = new Shop();
        ShopInfo resultShop = shop.searchShop(shopInfo);
        if (resultShop != null)
            return "{\"message\":\"success\", \"shopName\":\"" + resultShop.shopName + "\", \"shopID\":\"" + resultShop.shopID + "\", \"shopAddress\":\"" + resultShop.shopAddress + "\"}";
        else
            return "{\"message\":\"failed\"}";
    }

    public string SearchShopStocking(HttpRequest request)
    {
        ShopStockingInfo shopStockingInfo = new ShopStockingInfo();
        shopStockingInfo.shopID = request.Form["shopID"];
        shopStockingInfo.productID = request.Form["productID"];
        ShopStocking shopStocking = new ShopStocking();
        ShopStockingInfo resultShopStocking = shopStocking.searchShopStocking(shopStockingInfo);
        if (resultShopStocking != null)
        {
            return "{\"message\":\"success\", \"quantity\":\"" + resultShopStocking.quantity + "\"}";
        }
        else {return "{\"message\":\"failed\"}"; }

    }

    public string TransferProduct(HttpRequest request)
    {
        TransferProductInfo transferProductInfo = new TransferProductInfo();
        transferProductInfo.productID = request.Form["productID"];
        transferProductInfo.fromShopID = request.Form["fromshopID"];
        transferProductInfo.toShopID = request.Form["toshopID"];
        transferProductInfo.fromShopFinalQuantity = Convert.ToInt32(request.Form["fromshopfinalQuantity"]);
        transferProductInfo.toShopFinalQuantity = Convert.ToInt32(request.Form["toshopfinalQuantity"]);
        TransferProduct transferProduct = new TransferProduct();
        bool resultTransferProduct = transferProduct.transferProduct(transferProductInfo);
        if(resultTransferProduct)
            return "{\"message\":\"success\"}";
        else
            return "{\"message\":\"failed\"}";
    }

    public string JoinMembership(HttpRequest request)
    {
        MembershipInfo membershipInfo = new MembershipInfo();
        membershipInfo.firstname = request.Form["firstname"];
        membershipInfo.lastname = request.Form["lastname"];
        membershipInfo.contactnumber = request.Form["contactnumber"];
        membershipInfo.dateofbirth = request.Form["dateofbirth"];
        membershipInfo.country = request.Form["country"];
        membershipInfo.email = request.Form["email"];
        membershipInfo.role = Convert.ToChar(request.Form["role"]);
        Membership membership = new Membership();
        bool resultMembership = membership.joinMembership(membershipInfo);
        if(resultMembership)
            return "{\"message\":\"success\"}";
        else
            return "{\"message\":\"failed\"}";
    }

    public string SearchMembership(HttpRequest request)
    {
        MembershipInfo membershipInfo = new MembershipInfo();
        membershipInfo.membershipid = request.Form["membershipID"];
        membershipInfo.contactnumber = request.Form["contactNumber"];
        membershipInfo.email = request.Form["email"];
        Membership membership = new Membership();
        MembershipInfo resultMembershipInfo = membership.searchMembership(membershipInfo);
        if(resultMembershipInfo != null) {
            string membershipID = resultMembershipInfo.membershipid;
            string firstname = resultMembershipInfo.firstname;
            string lastname = resultMembershipInfo.lastname;
            string contactNumber = resultMembershipInfo.contactnumber;
            string dateofbirth = resultMembershipInfo.dateofbirth;
            string country = resultMembershipInfo.country;
            string email = resultMembershipInfo.email;
            string memberRole = ReadRole(resultMembershipInfo.role);
            return "{\"message\":\"success\", \"membershipid\": \"" + membershipID + "\", \"firstname\":\"" + firstname + "\", \"lastname\":\"" + lastname + "\", \"contactNumber\":\"" + contactNumber + "\", \"dateofbirth\":\"" + dateofbirth + "\", \"country\":\"" + country + "\", \"email\":\"" + email + "\", \"role\":\"" + memberRole + "\"}";
        }
        else {
            return "{\"message\":\"failed\"}";
        }
    }

    #region Custom Method
    protected string ReadRole(char role)
    {
        switch (role)
        {
            case 'P':
                return "Platinum";
            case 'G':
                return "Gold";
            case 'S':
                return "Silver";
            case 'B':
                return "Bronze";
            default:
                return "NoMembership";
        }
    }
    #endregion
}