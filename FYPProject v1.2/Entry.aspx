﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Entry.aspx.cs" Inherits="Entry" %>

<%@ Register Src="~/Controls/PublicReference.ascx" TagPrefix="uc1" TagName="PublicReference" %>
<%@ Register Src="~/Controls/Menu.ascx" TagPrefix="uc1" TagName="Menu" %>
<%@ Register Src="~/Controls/Footer.ascx" TagPrefix="uc1" TagName="Footer" %>
<%@ Register Src="~/Controls/CountryControl.ascx" TagPrefix="uc1" TagName="CountryControl" %>



<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <uc1:PublicReference runat="server" id="PublicReference" />

    <script>
        $(function () {
            $home = $("#home");
            $entry = $("#entry");
            $quantityTxt = $("#quantityTxt");
            $home.removeClass("active");
            $entry.addClass("active");

            $quantityTxt.focusout(function (e) {
                if ($quantityTxt.val() > 999) {
                    $quantityTxt.val("999");
                }


            });

            // Check Login
            if (getCookie("staffID") == "") {
                window.location = "Login.aspx";
            }
            else {
                var staffName = getCookie("staffName");
                $("#staffNameUserBar").html("<a href=\"#\">Welcome, " + staffName + "</a>   ");
                $("#checkLogin").attr("href", "Login.aspx");
                $("#checkLogin").text("Logout");
            }
            $("#checkLogin").click(function () {
                setCookie("staffID", "", 0)

            });
            // /Check Login


            var staffID = getCookie("staffID");
            var staffName = getCookie("staffName");
            $("#staffIDText").val(staffID);
            $("#staffNameText").val(staffName);


        });
    </script>




</head>
<body>
    <div id="container">
    <div id="header">
        <uc1:Menu runat="server" id="Menu" />
    </div>

    <div id="body">
         <div id="navigationLocation">
            <h6><span style="padding-left: 5%">E-Sales > Insert Entry</span></h6>
            <hr />
            <h2><span style="margin-left: 5%;">Insert Entry</span></h2>
        </div>

<div class="container" style="margin-top: 5%; margin-bottom: 5%;">

    <div class="row">
        <div class="form-group col-sm-3">
            <label for="StaffID">Staff ID</label>
            <input id="staffIDText" type="text" value="" class="form-control" disabled />
        </div>
        <div class="form-group col-sm-3">
            <label for="StaffID">Staff Name</label>
            <input id="staffNameText" type="text" class="form-control" value="" disabled />
        </div>
 <%--   </div>

    <div class="row">--%>
        <div class="form-group col-sm-3">
            <label for="CustomerRegion">Customer Region</label>
            <select class="form-control">
               <uc1:CountryControl runat="server" id="CountryControl" />
            </select>
        </div>
            <div class="form-group col-sm-3">
            <label for="CustomerAge">Customer Age</label>
            <select class="form-control">
                <option value="none">Please select an age group</option>
                <option value="firstgp">10 - 20</option>
                <option value="secondgp">21 - 30</option>
                <option value="thirdgp">31 - 40</option>
                <option value="fourthgp">41 - 50</option>
                <option value="fifthgp">51 - 60</option>
                <option value="lastgp">Above 60</option>
            </select>
        </div>
    </div>

    <div class="row">
        <div class="form-group col-sm-6">
            <label for="ProductID">Product ID</label>
            <input type="text" class="form-control" />
        </div>
        <div class="form-group col-sm-6">
            <label for="ProductName">Product Name</label>
            <input type="text" class="form-control" disabled/>
        </div>
    </div>
                 
    <div class="row">
        <div class="form-group col-sm-6">
            <label for="quantity">Quantity</label>
            <input type="number" id="quantityTxt" value="1" class="form-control" min="1" max="999" />
        </div>
        <div class="form-group col-sm-6">
            <label for="price">Price</label>
            <input type="text" id="priceTxt" class="form-control" />
        </div>
    </div>

    <div class="row">
        <div class="form-group col-sm-6">
            <label for="discount">Discount</label>
            <input type="text" id="discountTxt" class="form-control" />
        </div>

        <div class="form-group col-sm-6">
            <label for="finalPrice">Final Price</label>
            <input type="text" id="finalPriceTxt" class="form-control" />
        </div>
    </div>




                <div class="form-group col-sm-2 col-sm-offset-4">
                    <label for="spacing"></label>
                    <input type="button" id="submit" value="Submit" class="form-control btn btn-default" />
                </div>

                <div class="form-group col-sm-2">
                    <label for="spacing"></label>
                    <input type="button" id="cancelButton" value="Cancel" class="form-control btn btn-default" />
                </div>





        </div>

    </div>
    <div class="footer">
        <uc1:Footer runat="server" id="Footer1" />
    </div>



    </div>
</body>
</html>
