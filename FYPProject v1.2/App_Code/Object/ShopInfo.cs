﻿using System;
using System.Collections.Generic;
using System.Web;

/// <summary>
/// Summary description for ShopInfo
/// </summary>
public class ShopInfo
{
    public string shopID;
    public string shopAddress;
    public string shopName;
}