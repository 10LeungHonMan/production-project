﻿using System;
using System.Collections.Generic;
using System.Web;
using Jayrock.Json;
/// <summary>
/// Summary description for EntryInfo
/// </summary>
public class EntryInfo
{
    public string StaffID;
    public string StaffName;    
    public string CustomerRegion;
    public string CustomerAge;
    public string EntryItem;
    public string membershipID;
    public string discount;
    public string finalPrice;
    public string lastEntryItemID;
}