﻿using System;
using System.Collections.Generic;
using System.Web;

/// <summary>
/// Summary description for MembershipInfo
/// </summary>
public class MembershipInfo
{
    public string membershipid;
    public string firstname;
    public string lastname;
    public string contactnumber;
    public string dateofbirth;
    public string country;
    public string email;
    public char role;
}