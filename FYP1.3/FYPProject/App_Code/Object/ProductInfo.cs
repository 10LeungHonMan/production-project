﻿using System;
using System.Collections.Generic;
using System.Web;

/// <summary>
/// Summary description for ProductInfo
/// </summary>
public class ProductInfo
{
    public string productID;
    public string productName;
    public int productQuantity;
    public double productPrice;
    public string productBrand;
    public string shopID;
}