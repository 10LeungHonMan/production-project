﻿using System;
using System.Collections.Generic;
using System.Web;

/// <summary>
/// Summary description for Shop
/// </summary>
public class Shop
{
    public static readonly string SystemMailAccount = System.Configuration.ConfigurationManager.AppSettings["SystemMailAccount"];
    public static readonly string SystemMailDisplayName = System.Configuration.ConfigurationManager.AppSettings["SystemMailDisplayName"];
    public static readonly string SMTPServer = System.Configuration.ConfigurationManager.AppSettings["SMTPServer"];
    public static readonly int SMTPPortNo = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["SMTPPortNo"]);
    //public static readonly string SystemMailPassword = System.Configuration.ConfigurationManager.AppSettings["SystemMailPassword"];
    public static string SupervisorMailAccount = "";
    public static string SupervisorMailDisplayName = "";

    private DatabaseAccess dbAccess = new DatabaseAccess();

    public ShopInfo searchShop(ShopInfo shopInfo)
    {
        ShopInfo resultShop = new ShopInfo();

        string shopID = shopInfo.shopID;
        String sql = string.Format("SELECT * FROM [esales].[dbo].[Shop] where shop_id = '{0}'", shopID);
        resultShop.shopID = shopID;
        dbAccess.open();
        try
        {
            System.Data.DataTable dt = dbAccess.select(sql);
            if (dt.Rows.Count > 0)
            {
                resultShop.shopName = Convert.ToString(dt.Rows[0]["shop_name"]);
                resultShop.shopAddress = Convert.ToString(dt.Rows[0]["shop_address"]);
                resultShop.shopName = Convert.ToString(dt.Rows[0]["shop_name"]);
                return resultShop;
            }
            else
            {
                return null;
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            dbAccess.close();
        }
    }
}