﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="Login" %>

<%@ Register Src="~/Controls/PublicReference.ascx" TagPrefix="uc1" TagName="PublicReference" %>
<%@ Register Src="~/Controls/Menu.ascx" TagPrefix="uc1" TagName="Menu" %>
<%@ Register Src="~/Controls/Footer.ascx" TagPrefix="uc1" TagName="Footer" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">

    <title></title>
    <uc1:PublicReference runat="server" id="PublicReference" />

   
    <script>
        $(function () {
            $usernameTxt = $("#usernameTxt");
            $passwordTxt = $("#passwordTxt");
            $loginBtn = $("#loginBtn");
            $cancelBtn = $("#cancelBtn");

            if (getCookie("staffID") != "")
            {
                window.location = "/Home.aspx";
            }

            // Cancel Button Function
            $cancelBtn.click(function () {
                $usernameTxt.val("");
                $passwordTxt.val("");
            });
            
            $loginBtn.click(function () {
                var staffID = $usernameTxt.val();   
                var password = $passwordTxt.val();

                ajaxSubmit({
                    action: "Login",
                    staffID: staffID,
                    password: password,
                }, function (data) {
                    if (data.message == "success") {
                        window.alert("Login Success! Welcome, "+data.staffName);
                        window.location = "Home.aspx";
                        setCookie("staffID", data.staffID, 0.5);
                        setCookie("staffName", data.staffName, 0.5);
                    }
                    else {
                        window.alert("Login Failed!");
                    }
                });

            });

        });
    </script>



</head>
<body>
    <div id="container">
    <!-------------Header-------------------->
    <div id="header">
        <uc1:Menu runat="server" ID="Menu" />
    </div>
    <!----------------------Header--------------->

    <!--------------Body--------------->
    <div id="body">

        <div id="navigationLocation">
            <h6><span style="padding-left: 5%">E-Sales > Login</span></h6>
            <hr />
            <h2><span style="margin-left: 5%;">Login</span></h2>
        </div>

        <form role="form">

        <div class="form-group col-sm-4 col-sm-offset-4">
            <label for="username">Staff ID</label>
            <input type="text" id="usernameTxt" class="form-control"/>
        </div>

        <div class="col-sm-4"></div>

        <div class="form-group col-sm-4 col-sm-offset-4">
            <label for="password">Password</label>
            <input type="text" id="passwordTxt" class="form-control" />
        </div>

        <div class="col-sm-4"></div>

        <div class="form-group col-sm-2 col-sm-offset-4">
            <label for="none"></label>
            <input type="button" value="Login" class="btn btn-default form-control" id="loginBtn"/>
        </div>

        <div class="form-group col-sm-2">
            <label for="none"></label>
            <input type="button" value="Cancel" class="btn btn-default form-control" id="cancelBtn"/>
        </div>





            </form>


    </div>
    <!--------------Body--------------->
    <div id="footer">
        <uc1:Footer runat="server" id="Footer" />
    </div>




    </div>
</body>
</html>
