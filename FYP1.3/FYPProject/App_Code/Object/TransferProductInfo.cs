﻿using System;
using System.Collections.Generic;
using System.Web;

/// <summary>
/// Summary description for TransferProductInfo
/// </summary>
public class TransferProductInfo
{
    public string productID;
    public string fromShopID;
    public string toShopID;
    public int fromShopFinalQuantity;
    public int toShopFinalQuantity;

}