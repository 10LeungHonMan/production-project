﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SearchEntry.aspx.cs" Inherits="SearchEntry" %>

<%@ Register Src="~/Controls/PublicReference.ascx" TagPrefix="uc1" TagName="PublicReference" %>
<%@ Register Src="~/Controls/Menu.ascx" TagPrefix="uc1" TagName="Menu" %>
<%@ Register Src="~/Controls/Footer.ascx" TagPrefix="uc1" TagName="Footer" %>
<%@ Register Src="~/Controls/CountryControl.ascx" TagPrefix="uc1" TagName="CountryControl" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title> 
        <uc1:PublicReference runat="server" id="PublicReference" />
    <script>
        $(function () {
            // Check Login
            if (getCookie("staffID") == "") {
                window.location = "Login.aspx";
            }
            else {
                var staffName = getCookie("staffName");
                $("#staffNameUserBar").html("<a href=\"#\">Welcome, " + staffName + "</a>   ");
                $("#checkLogin").attr("href", "Login.aspx");
                $("#checkLogin").text("Logout");
            }
            $("#checkLogin").click(function () {
                setCookie("staffID", "", 0)

            });
            // /Check Login
        });
    </script>
</head>
<body>
        <div id="header">
        <uc1:Menu runat="server" id="Menu" />
    </div>
    <div id="body">
         <div id="navigationLocation">
            <h6><span style="padding-left: 5%">E-Sales > Search Entry</span></h6>
            <hr />
            <h2><span style="margin-left: 5%;">Search Entry</span></h2>
        </div>

<div class="container" style="margin-top: 5%; margin-bottom: 5%;">

    <div class="row">
        <div class="form-group col-sm-6 col-sm-offset-3">
            <label for="ProductID">Entry ID</label>
            <input type="text" class="form-control" id="productID" />
        </div>
    </div>


    <div class="row">
        <div class="form-group col-sm-2 col-sm-offset-4">
            <label for="spacing"></label>
            <input type="button" id="voidbtn" value="Void" class="form-control btn btn-default" />
        </div>
        
        <div class="form-group col-sm-2">
            <label for="spacing"></label>
            <input type="button" id="backButton" value="Back" class="form-control btn btn-default" />
        </div>
    </div>




        </div>

    </div>
    <div id="footer">
        <uc1:Footer runat="server" id="Footer1" />
    </div>




</body>
</html>
