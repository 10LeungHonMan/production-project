﻿using System;
using System.Collections.Generic;
using System.Web;
using Jayrock.Json;
/// <summary>
/// Summary description for Entry
/// </summary>
public class Entry
{
    public static readonly string SystemMailAccount = System.Configuration.ConfigurationManager.AppSettings["SystemMailAccount"];
    public static readonly string SystemMailDisplayName = System.Configuration.ConfigurationManager.AppSettings["SystemMailDisplayName"];
    public static readonly string SMTPServer = System.Configuration.ConfigurationManager.AppSettings["SMTPServer"];
    public static readonly int SMTPPortNo = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["SMTPPortNo"]);
    //public static readonly string SystemMailPassword = System.Configuration.ConfigurationManager.AppSettings["SystemMailPassword"];
    public static string SupervisorMailAccount = "";
    public static string SupervisorMailDisplayName = "";

    private DatabaseAccess dbAccess = new DatabaseAccess();

    public bool InsertEntry(EntryInfo entryInfo)
    {

        String sql = string.Format("INSERT INTO [esales].[dbo].[Invoice] (staffID, staffName, customerRegion, customerAge, membershipID, discount, finalPrice) VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}')", entryInfo.StaffID, entryInfo.StaffName, entryInfo.CustomerRegion, entryInfo.CustomerAge, entryInfo.membershipID, entryInfo.discount, entryInfo.finalPrice);
        String lastinsertrowsql = string.Format("SELECT TOP 1 invoice_id FROM [esales].[dbo].[Invoice] ORDER BY invoice_id DESC");
        dbAccess.open();
        try
        {
            dbAccess.update(sql);
            System.Data.DataTable dt = dbAccess.select(lastinsertrowsql);
            if (dt.Rows.Count > 0)
            {
                string lastinsertrowID = Convert.ToString(dt.Rows[0]["invoice_id"]);
                int lastentryID = Convert.ToInt32(entryInfo.lastEntryItemID);
                JsonArray entryObj = new JsonArray();
                entryObj = (JsonArray)Jayrock.Json.Conversion.JsonConvert.Import(entryInfo.EntryItem);
                for (int i = 0; i <= lastentryID; i++)
                {
                    string istring = Convert.ToString(i);
                    JsonObject jo = (JsonObject)entryObj[i];
                    string productID = Convert.ToString(jo["productID"]);
                    String sql2 = string.Format("INSERT INTO [esales].[dbo].[InvoiceItem] (parent_id, product_name, product_quantity, product_price, product_id) VALUES ('{0}', '{1}', '{2}', '{3}', '{4}')", lastinsertrowID);
                }
                
            }
            return true;
        }
        catch (Exception ex)
        {
            return false;
        }
        finally
        {
            dbAccess.close();
        }
    }
}