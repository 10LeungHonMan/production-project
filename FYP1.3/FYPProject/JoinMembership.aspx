﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="JoinMembership.aspx.cs" Inherits="JoinMembership" %>

<%@ Register Src="~/Controls/Menu.ascx" TagPrefix="uc1" TagName="Menu" %>
<%@ Register Src="~/Controls/PublicReference.ascx" TagPrefix="uc1" TagName="PublicReference" %>
<%@ Register Src="~/Controls/Footer.ascx" TagPrefix="uc1" TagName="Footer" %>
<%@ Register Src="~/Controls/CountryControl.ascx" TagPrefix="uc1" TagName="CountryControl" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
     <uc1:PublicReference runat="server" ID="PublicReference" />
    <script>
        $(function () {
            $dateOfBirth = $("#dateOfBirth");
            $dateOfBirth.pickadate({
                format: 'yyyy-mm-dd',
                selectYears: true,
                selectMonths: true
            });
            // Check Login
            if (getCookie("staffID") == "") {
                window.location = "Login.aspx";
            }
            else {
                var staffName = getCookie("staffName");
                $("#staffNameUserBar").html("<a href=\"#\">Welcome, " + staffName + "</a>   ");
                $("#checkLogin").attr("href", "Login.aspx");
                $("#checkLogin").text("Logout");
            }
            $("#checkLogin").click(function () {
                setCookie("staffID", "", 0)

            });
            // /Check Login

            $("#emailTxt").focusout(function () {
                if ($("#emailTxt").val().indexOf("@") < 0 || $("#emailTxt").val().indexOf(".") < 0)
                {
                    alert("Invalid email format.");
                }
            });


            $("#submitBtn").click(function () {
                if ($("#firstNameTxt").val() == "" || $("#lastNameTxt").val() == "" || $("#contactNumberTxt").val() == "" ||
                    $("#dateOfBirth").val() == "" || $("#countryTxt").val() == "" || $("#emailTxt").val() == "")
                {
                    alert("Please fill in the form above.");
                }
                else
                {
                    var firstname = $("#firstNameTxt").val();
                    var lastname = $("#lastNameTxt").val();
                    var contactnumber = $("#contactNumberTxt").val();
                    var dateofbirth = $("#dateOfBirth").val();
                    var country = $("#countryTxt").val();
                    var email = $("#emailTxt").val();
                    var role = $("#memberRole").val();
                    ajaxSubmit({
                        action: "JoinMembership",
                        firstname: firstname,
                        lastname: lastname,
                        contactnumber: contactnumber,
                        dateofbirth: dateofbirth,
                        country: country,
                        email: email,
                        role: role,
                    }, function (data) {
                        if (data.message == "success")
                        {
                            alert("Membership joined!");
                        }
                    });
                }
            });

        });
        
    </script>
</head>
<body>
    <div id="header">
        <uc1:Menu runat="server" ID="Menu" />
    </div>
    <div id="body">
         <div id="navigationLocation">
            <h5><span style="padding-left: 5%"><a href="Home.aspx" style="text-decoration:none;">E-Sales</a> > <a href="JoinMembership.aspx">Member</a> > Join Membership</span></h5>
            <hr />
            <h2><span style="margin-left: 5%;">Join Membership</span></h2>
        </div>

        <div class="container">
            <div class="row">
                <div class="form-group col-sm-4 col-sm-offset-2">
                    <label for="firstnameTxt">First Name</label>
                    <input type="text" id="firstNameTxt" class="form-control"/>
                </div>
                <div class="form-group col-sm-4">
                    <label for="firstnameTxt">Last Name</label>
                    <input type="text" id="lastNameTxt" class="form-control"/>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-sm-4 col-sm-offset-2">
                    <label for="firstnameTxt">Contact Number</label>
                    <input type="text" id="contactNumberTxt" class="form-control"/>
                </div>
                <div class="form-group col-sm-4">
                    <label for="firstnameTxt">Date of Birth</label>
                    <input type="date" id="dateOfBirth" class="form-control"/>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-sm-4 col-sm-offset-2">
                    <label for="firstnameTxt">Country</label>
                    <select id="countryTxt" class="form-control">
                        <uc1:CountryControl runat="server" ID="CountryControl" />
                    </select>
                </div>
                <div class="form-group col-sm-4">
                    <label for="firstnameTxt">E-mail</label>
                    <input type="email" id="emailTxt" class="form-control"/>
                </div>
            </div>

            <div class="row">
                <div class="form-group col-sm-4 col-sm-offset-4">
                    <label for="memberroleTxt">Role</label>
                    <select id="memberRole" class="form-control" >
                        <option value="P">Platinum</option>
                        <option value="G">Gold</option>
                        <option value="S">Silver</option>
                        <option value="B">Bronze</option>
                    </select>
                </div>
            </div>

            <div class="form-group col-sm-2 col-sm-offset-4">
                    <label for="spacing"></label>
                    <input type="button" id="submitBtn" value="Submit" class="form-control btn btn-primary" />
                </div>

                <div class="form-group col-sm-2">
                    <label for="spacing"></label>
                    <input type="button" id="backButton" value="Back" class="form-control btn btn-default" />
                </div>

        </div>

    </div>
     <div class="footer">
        <uc1:Footer runat="server" id="Footer1" />
    </div>
</body>
</html>
