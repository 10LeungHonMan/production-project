﻿$(function () {
    $.ajaxSetup({ cache: true });
});


//function for ajax submit
var ajaxSubmit = function (formData, callback, postUrl, errorcallback) {

    waitServerToResponse = true;

    $.ajax({
        type: 'POST',
        url: (!postUrl ? "HttpHandler/AjaxHandler.ashx" : postUrl), 
        data: formData, 
        success: function (data) {

            if (data.error) { 
                //alert(data.error);
            }
            else
            { 
                if (callback) {
                    callback(data);
                }
            }

        },
        error: function (data) { 

            waitServerToResponse = false;

            //if ($progressBar)
            //    $progressBar.hideProgressBar();

            if (data.responseText) {
                if (data.responseText.indexOf("<html>") >= 0)
                {
                    //alert("");
                    data.message = "System Error";
                }
                else
                {
                  //  alert(data.responseText);

                }
            } else if (data.error) {
                alert(data.error);
            }
            else {
                alert('Failed to retrive data from server.');
            }
             
            if (errorcallback) {
                errorcallback(data);
            }
            window.location = "Login.aspx";
        } 
    });
}

