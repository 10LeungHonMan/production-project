﻿using System;
using System.Collections.Generic;
using System.Web;

/// <summary>
/// Summary description for ShopStocking
/// </summary>
public class ShopStocking
{

    public static readonly string SystemMailAccount = System.Configuration.ConfigurationManager.AppSettings["SystemMailAccount"];
    public static readonly string SystemMailDisplayName = System.Configuration.ConfigurationManager.AppSettings["SystemMailDisplayName"];
    public static readonly string SMTPServer = System.Configuration.ConfigurationManager.AppSettings["SMTPServer"];
    public static readonly int SMTPPortNo = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["SMTPPortNo"]);
    //public static readonly string SystemMailPassword = System.Configuration.ConfigurationManager.AppSettings["SystemMailPassword"];
    public static string SupervisorMailAccount = "";
    public static string SupervisorMailDisplayName = "";

    private DatabaseAccess dbAccess = new DatabaseAccess();

    public ShopStockingInfo searchShopStocking(ShopStockingInfo shopStockingInfo)
    {
        ShopStockingInfo resultShopStocking = new ShopStockingInfo();

        string shopID = shopStockingInfo.shopID;
        string productID = shopStockingInfo.productID;
        String sql = string.Format("SELECT * FROM [esales].[dbo].[Shop_Stocking] where shop_id = '{0}' AND product_id = '{1}'", shopID, productID);
        resultShopStocking.shopID = shopID;
        resultShopStocking.productID = productID;
        dbAccess.open();
        try
        {
            System.Data.DataTable dt = dbAccess.select(sql);
            if (dt.Rows.Count > 0)
            {
                resultShopStocking.quantity = Convert.ToInt32(dt.Rows[0]["quantity"]);
                return resultShopStocking;
            }
            else
            {
                return null;
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            dbAccess.close();
        }
    }

}