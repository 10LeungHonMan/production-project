﻿    <%@ Page Language="C#" AutoEventWireup="true" CodeFile="Home.aspx.cs" Inherits="Home" %>

<%@ Register Src="~/Controls/PublicReference.ascx" TagPrefix="uc1" TagName="PublicReference" %>
<%@ Register Src="~/Controls/Menu.ascx" TagPrefix="uc1" TagName="Menu" %>
<%@ Register Src="~/Controls/Footer.ascx" TagPrefix="uc1" TagName="Footer" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
        <uc1:PublicReference runat="server" id="PublicReference" />

    <script>
        $(function () {
            // Check Login
            if (getCookie("staffID") == "") {
                window.location = "Login.aspx";
            }
            else {
                var staffName = getCookie("staffName");
                $("#staffNameUserBar").html("<a href=\"#\">Welcome, " + staffName + "</a>   ");
                $("#checkLogin").attr("href", "Login.aspx");
                $("#checkLogin").text("Logout");
            }
            $("#checkLogin").click(function () {
                setCookie("staffID", "", 0)

            });
            // /Check Login
        });

    </script>


</head>
<body>
    <div id="header">
        <uc1:Menu runat="server" id="Menu" />
    </div>
    <div id="body">
         <div id="navigationLocation">
            <h5><span style="padding-left: 5%"><a href="Home.aspx" style="text-decoration:none;">E-Sales</a> > Home</span></h5>
            <hr />
            <h2><span style="margin-left: 5%;">Home</span></h2>
        </div>
        <div class="container">
        <div style="margin-top: 5%;" class="jumbotron">
            Welcome to e-Sales system. This is a system that allow user to create invoice, 
            distribute products to diff erent shop and manage membership.



        </div>
            </div>
    </div>
    <div class="footer">
        <uc1:Footer runat="server" id="Footer1" />
    </div>
</body>
</html>
