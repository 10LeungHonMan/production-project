﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SearchProduct.aspx.cs" Inherits="SearchProduct" %>

<%@ Register Src="~/Controls/PublicReference.ascx" TagPrefix="uc1" TagName="PublicReference" %>
<%@ Register Src="~/Controls/Menu.ascx" TagPrefix="uc1" TagName="Menu" %>
<%@ Register Src="~/Controls/Footer.ascx" TagPrefix="uc1" TagName="Footer" %>
<%@ Register Src="~/Controls/CountryControl.ascx" TagPrefix="uc1" TagName="CountryControl" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title> 
        <uc1:PublicReference runat="server" id="PublicReference" />
    <script>
        $(function () {

            // Check Login
            if (getCookie("staffID") == "") {
                window.location = "Login.aspx";
            }
            else {
                var staffName = getCookie("staffName");
                $("#staffNameUserBar").html("<a href=\"#\">Welcome, " + staffName + "</a>   ");
                $("#checkLogin").attr("href", "Login.aspx");
                $("#checkLogin").text("Logout");
            }
            $("#checkLogin").click(function () {
                setCookie("staffID", "", 0)

            });
            // /Check Login

            $("#productInfo").hide();

            $("#searchButton").click(function () {
                var productID = $("#productID").val().replace(/ /g, '');
                var shopID = $("#shopID").val();
                ajaxSubmit({
                    action: "SearchProductByShopID",
                    productID: productID,
                    shopID: shopID,
                }, function (data) {
                    if (data.message == "success") {
                        $("#noResult").hide();
                        $("#productName").val(data.productName);
                        $("#productBrand").val(data.productBrand);
                        $("#productPrice").val(data.productPrice);
                        $("#productQuantity").val(data.productQuantity);
                        $("#productInfo").show(300);
                    }
                    else {
                        $("#productInfo").hide(300);
                        $("#noResult").show(300);
                    }
                }
                )
            

            });

            $("#shopID").focusout(function () {
                var shopID = $("#shopID").val();
                if (shopID != "") {
                        ajaxSubmit({
                            action: "SearchShop",
                            shopID: shopID,
                        }, function (data) {
                            if (data.message != "success") {
                                alert("Shop not found.");
                                $("#shopID").val("");
                            }
                        });
                }
            });


        });
       
    </script>
</head>
<body>
        <div id="header">
        <uc1:Menu runat="server" id="Menu" />
    </div>
    <div id="body">
         <div id="navigationLocation">
            <h6><span style="padding-left: 5%">E-Sales > Product > Search Product</span></h6>
            <hr />
            <h2><span style="margin-left: 5%;">Search Product</span></h2>
        </div>

<div class="container" style="margin-top: 5%; margin-bottom: 5%;">

    <div class="row">
        <div class="form-group col-sm-4 col-sm-offset-2">
            <label for="ProductID">Product ID</label>
            <input type="text" class="form-control" id="productID" />
        </div>
        <div class="form-group col-sm-4">
            <label for="ProductID">Shop ID</label>
            <input type="text" class="form-control" id="shopID" />
        </div>
    </div>

    <div id="noResult">
        <center><h1>No Result.</h1></center>
    </div>




<div id="productInfo">

    <div class="row">
        <div class="form-group col-sm-4 col-sm-offset-2">
            <label for="ProductName">Product Name</label>
            <input type="text" class="form-control" id="productName" disabled/>
        </div>
        <div class="form-group col-sm-4">
            <label for="ProductBrand">Product Brand</label>
            <input type="text" class="form-control" id="productBrand" disabled />
        </div>
    </div>

    <div class="row">
        <div class="form-group col-sm-4 col-sm-offset-2">
            <label for="ProductPrice">Product Price</label>
            <input type="text" class="form-control" id="productPrice" disabled/>
        </div>
        <div class="form-group col-sm-4">
            <label for="Product">Product Quantity</label>
            <input type="text" class="form-control" id="productQuantity" disabled />
        </div>
    </div>

</div>






    <div class="row">
        <div class="form-group col-sm-2 col-sm-offset-5">
            <label for="spacing"></label>
            <input type="button" id="searchButton" value="Search" class="form-control btn btn-default btn-info" />
        </div>
    </div>




        </div>
        </div>

    <div class="footer">
        <uc1:Footer runat="server" id="Footer1" />
    </div>




</body>
</html>
