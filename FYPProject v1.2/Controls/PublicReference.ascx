﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PublicReference.ascx.cs" Inherits="Controls_PublicReference" %>

<!-- CSS -->
<link rel="stylesheet" type="text/css" href="../Resources/CSS/bootstrap.css" />
<link rel="stylesheet" type="text/css" href="../Resources/CSS/bootstrap-theme.css" />
<link href="../Resources/CSS/FooterControl.css" rel="stylesheet" />
<link href="../Resources/CSS/Main.css" rel="stylesheet" />
<link href="../Resources/CSS/Pickadate/default.css" rel="stylesheet" />
<link href="../Resources/CSS/Pickadate/default.date.css" rel="stylesheet" />
<link href="../Resources/CSS/Pickadate/default.time.css" rel="stylesheet" />

<!-- Javascript -->
<script src="../Resources/Javascripts/jquery-1.12.3.js"></script>
<script src="../Resources/Javascripts/bootstrap.js"></script>
<script src="../Resources/Javascripts/ajaxSubmit.js"></script>
<script src="../Resources/Javascripts/Pickadate/picker.js"></script>
<script src="../Resources/Javascripts/Pickadate/picker.date.js"></script>
<script src="../Resources/Javascripts/Pickadate/picker.time.js"></script>
<script src="../Resources/Javascripts/Cookies.js"></script>