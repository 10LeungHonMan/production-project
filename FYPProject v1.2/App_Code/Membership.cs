﻿using System;
using System.Collections.Generic;
using System.Web;

/// <summary>
/// Summary description for Membership
/// </summary>
public class Membership
{
    public static readonly string SystemMailAccount = System.Configuration.ConfigurationManager.AppSettings["SystemMailAccount"];
    public static readonly string SystemMailDisplayName = System.Configuration.ConfigurationManager.AppSettings["SystemMailDisplayName"];
    public static readonly string SMTPServer = System.Configuration.ConfigurationManager.AppSettings["SMTPServer"];
    public static readonly int SMTPPortNo = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["SMTPPortNo"]);
    //public static readonly string SystemMailPassword = System.Configuration.ConfigurationManager.AppSettings["SystemMailPassword"];
    public static string SupervisorMailAccount = "";
    public static string SupervisorMailDisplayName = "";

    private DatabaseAccess dbAccess = new DatabaseAccess();

    public bool joinMembership(MembershipInfo membershipInfo)
    {
        String sql = string.Format("INSERT INTO [esales].[dbo].[Membership] (member_firstname, member_lastname, member_contact, member_dob, member_country, member_email, member_role) VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}')", membershipInfo.firstname, membershipInfo.lastname, membershipInfo.contactnumber, membershipInfo.dateofbirth, membershipInfo.country, membershipInfo.email, membershipInfo.role);
        dbAccess.open();
        try
        {
            dbAccess.update(sql);
            return true;
        }
        catch (Exception ex)
        {
            return false;
        }
        finally
        {
            dbAccess.close();
        }
    }

    public MembershipInfo searchMembership(MembershipInfo membershipInfo)
    {
        MembershipInfo resultMembershipInfo = new MembershipInfo();
        string membershipID = membershipInfo.membershipid;
        string contactNumber = membershipInfo.contactnumber;
        string email = membershipInfo.email;
        
        String sql = string.Format("SELECT * FROM [esales].[dbo].[Membership] WHERE membership_id = '{0}' OR member_contact = '{1}' OR member_email = '{2}'", membershipID, contactNumber, email);
        dbAccess.open();
        try
        {
            System.Data.DataTable dt = dbAccess.select(sql);
            if (dt.Rows.Count > 0)
            {
                resultMembershipInfo.membershipid = Convert.ToString(dt.Rows[0]["membership_id"]);
                resultMembershipInfo.firstname = Convert.ToString(dt.Rows[0]["member_firstname"]);
                resultMembershipInfo.lastname = Convert.ToString(dt.Rows[0]["member_lastname"]);
                resultMembershipInfo.contactnumber = Convert.ToString(dt.Rows[0]["member_contact"]);
                resultMembershipInfo.dateofbirth = Convert.ToString(dt.Rows[0]["member_dob"]);
                resultMembershipInfo.country = Convert.ToString(dt.Rows[0]["member_country"]);
                resultMembershipInfo.email = Convert.ToString(dt.Rows[0]["member_email"]);
                resultMembershipInfo.role = Convert.ToChar(dt.Rows[0]["member_role"]);
                return resultMembershipInfo;
            }
            else
            {
                return null;
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            dbAccess.close();
        }
    }

}