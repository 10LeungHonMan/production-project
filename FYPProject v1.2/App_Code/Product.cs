﻿using System;
using System.Collections.Generic;
using System.Web;

/// <summary>
/// Summary description for Product
/// </summary>
public class Product
{
    public static readonly string SystemMailAccount = System.Configuration.ConfigurationManager.AppSettings["SystemMailAccount"];
    public static readonly string SystemMailDisplayName = System.Configuration.ConfigurationManager.AppSettings["SystemMailDisplayName"];
    public static readonly string SMTPServer = System.Configuration.ConfigurationManager.AppSettings["SMTPServer"];
    public static readonly int SMTPPortNo = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["SMTPPortNo"]);
    //public static readonly string SystemMailPassword = System.Configuration.ConfigurationManager.AppSettings["SystemMailPassword"];
    public static string SupervisorMailAccount = "";
    public static string SupervisorMailDisplayName = "";

    private DatabaseAccess dbAccess = new DatabaseAccess();

    public ProductInfo searchProduct(ProductInfo productInfo)
    {
        ProductInfo resultProduct = new ProductInfo();

        string productID = productInfo.productID;
        String sql = string.Format("SELECT * FROM [esales].[dbo].[Product] where product_id = '{0}'", productID);

        dbAccess.open();
        try
        {
            System.Data.DataTable dt = dbAccess.select(sql);
            if (dt.Rows.Count > 0)
            {
                resultProduct.productID = Convert.ToString(dt.Rows[0]["product_id"]);
                resultProduct.productName = Convert.ToString(dt.Rows[0]["product_name"]);
                resultProduct.productQuantity = Convert.ToInt32(dt.Rows[0]["product_quantity"]);
                resultProduct.productPrice = Convert.ToDouble(dt.Rows[0]["product_price"]);
                resultProduct.productBrand = Convert.ToString(dt.Rows[0]["product_brand"]);
                return resultProduct;
            }
            else
            {
                return null;
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            dbAccess.close();
        }
    }

    public ProductInfo searchProductByID(ProductInfo productInfo)
    {
        ProductInfo resultProduct = new ProductInfo();

        string productID = productInfo.productID;
        string shopID = productInfo.shopID;
        String sql = string.Format("SELECT * FROM [esales].[dbo].[Product] where product_id = '{0}'", productID);
        string sql2 = string.Format("SELECT * FROM [esales].[dbo].[Shop_Stocking] where product_id = '{0}' AND shop_id = '{1}'", productID, shopID);
        dbAccess.open();
        try
        {
            System.Data.DataTable dt = dbAccess.select(sql);
            System.Data.DataTable dt2 = dbAccess.select(sql2);
            if (dt.Rows.Count > 0 && dt2.Rows.Count > 0)
            {
                resultProduct.productID = Convert.ToString(dt.Rows[0]["product_id"]);
                resultProduct.productName = Convert.ToString(dt.Rows[0]["product_name"]);
                resultProduct.productPrice = Convert.ToDouble(dt.Rows[0]["product_price"]);
                resultProduct.productBrand = Convert.ToString(dt.Rows[0]["product_brand"]);
                resultProduct.productQuantity = Convert.ToInt32(dt2.Rows[0]["quantity"]);
                return resultProduct;
            }
            else
            {
                return null;
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            dbAccess.close();
        }
    }

}