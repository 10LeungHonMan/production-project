﻿using System;
using System.Collections.Generic;

using System.Web;
using System.Security.Cryptography;
using System.Text;
using Jayrock.Json;


using System.Net;
using System.Net.Mail;

/// <summary>
/// Summary description for Staff
/// </summary>
public class Staff
{

    public static readonly string SystemMailAccount = System.Configuration.ConfigurationManager.AppSettings["SystemMailAccount"];
    public static readonly string SystemMailDisplayName = System.Configuration.ConfigurationManager.AppSettings["SystemMailDisplayName"];
    public static readonly string SMTPServer = System.Configuration.ConfigurationManager.AppSettings["SMTPServer"];
    public static readonly int SMTPPortNo = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["SMTPPortNo"]);
    //public static readonly string SystemMailPassword = System.Configuration.ConfigurationManager.AppSettings["SystemMailPassword"];
    public static string SupervisorMailAccount = "";
    public static string SupervisorMailDisplayName = "";
    
    private DatabaseAccess dbAccess = new DatabaseAccess();

    public StaffInfo login (StaffInfo staffInfo)
    {
        StaffInfo resultStaff = new StaffInfo();
        
        string staffID = staffInfo.staffID;
        string staffPassword = staffInfo.staffPassword;
        String sql = string.Format("SELECT * FROM [Staff] where staffID='{0}' AND staffPassword='{1}'", staffID, staffPassword);

        Dictionary<string, object> dict = new Dictionary<string, object>();
        dbAccess.open();
        try
        {
            System.Data.DataTable dt = dbAccess.select(sql);
            if (dt.Rows.Count > 0)
            {
                resultStaff.staffID = Convert.ToString(dt.Rows[0]["staffID"]);
                resultStaff.staffName = Convert.ToString(dt.Rows[0]["staffName"]);
                return resultStaff;
            }
            else
            {
                return null;
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            dbAccess.close();
        }
    }
}
