﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Menu.ascx.cs" Inherits="Controls_Menu" %>

<meta name="viewport" content="width=device-width, initial-scale=1.0">

<div class="panel" style="margin-top: 2%; margin-bottom: 2%; height: 5%;">
    <img src="../Resources/Image/google.png" style="width: 10%; height:5%; margin-left: 5%;"/>
</div>


<nav class="navbar navbar-inverse" style="border-radius: 0px;">
    <div class="container-fluid" style="padding-left: 5%; padding-right: 5%;" >
        <div class="navbar-header" style="">
               <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
            </div>
                  <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav">
               <li id="home" class="active"><a href="Home.aspx">Home</a></li>
               
                 <li id="epersonnel" class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">Invoice <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="Entry.aspx">Insert Invoice</a></li>
                        <li><a href="SearchEntry.aspx">Search Invoice</a></li>
                    </ul>
                </li>
                <li id="eleave">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">Product<span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="TransferProduct.aspx">Product Transfer</a></li>
                        <li><a href="SearchProduct.aspx">Search Product</a></li>
                    </ul>
                </li>
                <li id="config" class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">Member <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="JoinMembership.aspx">Join Membership</a></li>
                        <li><a href="SearchMembership.aspx " target="_blank">Search Membership</a></li>
                    </ul>
                </li>
            </ul>
            <ul id="UserBar" class="nav navbar-nav navbar-right">
                <li id="staffNameUserBar">
                </li>
                
                <li id="userinformation">
                   <a id="checkLogin" href="Login.aspx">Login</a>
                </li>
            </ul>
                      </div>
        </div>
    </nav>
